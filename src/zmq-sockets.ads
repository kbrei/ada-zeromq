with Interfaces.C; use Interfaces.C;
with System;
with ZMQ.Errno; use ZMQ.Errno;
with ZMQ.Flags; use ZMQ.Flags;
with ZMQ.Contexts; use ZMQ.Contexts;

package ZMQ.Sockets is
   type Socket is new System.Address;
   type Address is new String;

   type Socket_Type is
     (Pair,
      Pubish, Subscribe,
      Request, Reply,
      Dealer, Router,
      Pull, Push,
      XPublish, XSubscribe);

   function Create
     (Context : ZMQ.Contexts.Context;
      Sock_Type : Socket_Type)
     return Socket;

   function Close
     (Socket : ZMQ.Sockets.Socket)
     return Error;

   function Bind
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
     return Error;

   function Unbind
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
     return Error;

   function Connect
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
     return Error;

   function Disconnect
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
     return Error;

   function Send
     (Socket : ZMQ.Sockets.Socket;
      Data : not null access constant Bytes;
      Bytes_Send : out Integer;
      Flags : Send_Recv_Flags := No_Send_Recv_Flags)
     return Error;

   function Send_Const
     (Socket : ZMQ.Sockets.Socket;
      Data : not null access constant Bytes;
      Flags : Send_Recv_Flags := No_Send_Recv_Flags;
      Bytes_Send : out Integer)
     return Error;

   function Receive
     (Socket : ZMQ.Sockets.Socket;
      Data : not null access Bytes;
      Maximum_Bytes : Size;
      Bytes_Received : out Integer;
      Flags : Send_Recv_Flags := No_Send_Recv_Flags)
     return Error
   --  prevent truncation
   with Pre => Data'Length >= Maximum_Bytes;

   ---------------------------
   --  Socket options stuff --
   ---------------------------

   --  We define one big Socket_Option containing all ZMQ_... socket options
   --  Then we separate them by type and create
   --  enums of a subset of Socket_Option (with Static_Predicate).
   --  This enables us to overload the Get and Set functions
   --  and make sure that we never set/get an option to a value of the wrong
   --  type/size.

   --  We only support v4.0 options

   --  Some options have been renamed for clarity and consistency.
   --  But it's still be obvious how to map them
   --  to the corresponding values in zmq.h/man 3 zmq_setsockopts

   type Socket_Option is
     (Invalid_Option,
      Affinity, --  4.0, uint64
      Identity, --  4.0, blob 1-225
      Subscribe, --  4.0, blob
      Unsubscribe, --  4.0, blob
      Rate, --  4.0, int
      Recovery_Interval, --  4.0, int
      Send_Buffer_Size, --  4.0, int
      Receive_Buffer_Size, --  4.0, int
      Receive_More, --  4.0, bool
      File_Descriptor, --  4.0, int
      Events, --  4.0, Poll_Flags
      Sock_Type, -- 4.0, socket_type
      Linger, --  4.0, int
      Reconnect_Interval, --  4.0, int
      Backlog, --  4.0, int
      Reconnect_Interval_Max, --  4.0, int
      Max_Message_Size, --  4.0, int64_t
      Send_High_Water_Mark, --  4.0, int
      Receive_High_Water_Mark, --  4.0, int
      Multicast_Hops, --  4.0, int
      Receive_Time_Out, --  4.0, int
      Send_Time_Out, --  4.0, int
      Last_Endpoint, -- 4.0, string
      Router_Mandatory, --  4.0, bool
      TCP_Keepalive, --  4.0, OS bool
      TCP_Keepalive_Count, --  4.0, OS int
      TCP_Keepalive_Idle, --  4.0, OS int
      TCP_Keepalive_Interval, --  4.0, OS int
      TCP_Accept_Filter, --  4.0, blob TODO: it's in aliases!
      Immediate, --  4.0, bool
      XPub_Verbose, --  4.0, bool
      Router_Raw, --  4.0, bool
      IPV6, --  4.0, bool
      Mechanism, --  4.0, Security_Mechanism
      Plain_Server, --  4.0, bool
      Plain_Username, --  4.0, string
      Plain_Password, --  4.0, string
      Curve_Server, --  4.0, bool
      Curve_Publickey, --  4.0, blob/z85
      Curve_Secretkey, --  4.0, blob/z85
      Curve_Serverkey, --  4.0, blob/z85
      Probe_Router, --  4.0, bool
      Req_Correlate, --  4.0, bool
      Req_Relaxed, --  4.0, bool
      Conflate, --  4.0, bool
      ZAP_Domain) --  4.0, string
   with Convention => C;

   --  Values taken from macros in zmq.h

   for Socket_Option'Size use int'Size;
   for Socket_Option'Alignment use int'Alignment;
   for Socket_Option use
     (Invalid_Option => -1,
      Affinity => 4,
      Identity => 5,
      Subscribe => 6,
      Unsubscribe => 7,
      Rate => 8,
      Recovery_Interval => 9,
      Send_Buffer_Size => 11,
      Receive_Buffer_Size => 12,
      Receive_More => 13,
      File_Descriptor => 14,
      Events => 15,
      Sock_Type => 16,
      Linger => 17,
      Reconnect_Interval => 18,
      Backlog => 19,
      Reconnect_Interval_Max => 21,
      Max_Message_Size => 22,
      Send_High_Water_Mark => 23,
      Receive_High_Water_Mark => 24,
      Multicast_Hops => 25,
      Receive_Time_Out => 27,
      Send_Time_Out => 28,
      Last_Endpoint => 32,
      Router_Mandatory => 33,
      TCP_Keepalive => 34,
      TCP_Keepalive_Count => 35,
      TCP_Keepalive_Idle => 36,
      TCP_Keepalive_Interval => 37,
      TCP_Accept_Filter => 38,
      Immediate => 39,
      XPub_Verbose => 40,
      Router_Raw => 41,
      IPV6 => 42,
      Mechanism => 43,
      Plain_Server => 44,
      Plain_Username => 45,
      Plain_Password => 46,
      Curve_Server => 47,
      Curve_Publickey => 48,
      Curve_Secretkey => 49,
      Curve_Serverkey => 50,
      Probe_Router => 51,
      Req_Correlate => 52,
      Req_Relaxed => 53,
      Conflate => 54,
      ZAP_Domain => 55);

   --  Extra types for some of the options
   --  They map to ints in ZMQ, but
   --  We strive for more type-safety in this binding.

   type Security_Mechanism is
     (Null_Mechanism, Plain, Curve)
     with Convention => C;
   for Security_Mechanism'Size use int'Size;
   for Security_Mechanism'Alignment use int'Alignment;
   for Security_Mechanism use
     (Null_Mechanism => 0,
      Plain => 1,
      Curve => 2);

   type OS_Boolean is (OS_Default, True, False);
   type OS_Integer is new int range -1 .. int'Last;

   type Integer_Socket_Option is new Socket_Option
   with Static_Predicate => Integer_Socket_Option in
      Rate |
      Recovery_Interval |
      Send_Buffer_Size |
      Receive_Buffer_Size |
      File_Descriptor |
      Linger |
      Reconnect_Interval |
      Backlog |
      Reconnect_Interval_Max |
      Send_High_Water_Mark |
      Receive_High_Water_Mark |
      Multicast_Hops |
      Receive_Time_Out |
      Send_Time_Out;

   type Integer_64_Socket_Option is new Socket_Option
   with Static_Predicate => Integer_64_Socket_Option in
      Max_Message_Size;

   --  TODO: proper Affinity bitmap type
   type Unsigned_64_Socket_Option is new Socket_Option
   with Static_Predicate => Unsigned_64_Socket_Option in
      Affinity;

   type Boolean_Socket_Option is new Socket_Option
   with Static_Predicate => Boolean_Socket_Option in
      Receive_More |
      Router_Mandatory |
      Immediate |
      XPub_Verbose |
      Router_Raw |
      IPV6 |
      Probe_Router |
      Req_Correlate |
      Req_Relaxed |
      Conflate |
      Plain_Server;

   type OS_Integer_Socket_Option is new Socket_Option
   with Static_Predicate => OS_Integer_Socket_Option in
      TCP_Keepalive_Count |
      TCP_Keepalive_Idle |
      TCP_Keepalive_Interval;

   type OS_Boolean_Socket_Option is new Socket_Option
   with Static_Predicate => OS_Boolean_Socket_Option in
      TCP_Keepalive;

   type Mechanism_Socket_Option is new Socket_Option
   with Static_Predicate => Mechanism_Socket_Option in
      Mechanism;

   type Events_Socket_Option is new Socket_Option
   with Static_Predicate => Events_Socket_Option in
      Events;

   type Type_Socket_Option is new Socket_Option
   with Static_Predicate => Type_Socket_Option in
      Sock_Type;

   type Bytes_Socket_Option is new Socket_Option
   with Static_Predicate => Bytes_Socket_Option in
      Identity |
      Subscribe |
      Unsubscribe |
      TCP_Accept_Filter |
      Curve_Publickey |
      Curve_Secretkey |
      Curve_Serverkey;

   type String_Socket_Option is new Socket_Option
   with Static_Predicate => String_Socket_Option in
      Last_Endpoint |
      Plain_Username |
      Plain_Password |
      Curve_Publickey |
      Curve_Secretkey |
      Curve_Serverkey |
      ZAP_Domain;

   ----------------
   -- Predicates --
   ----------------

   --  Some Socket_Options do not allow
   --  arbitrary length data

   function Valid_Length
     (Option : String_Socket_Option;
      Value : String)
     return Boolean;

   function Valid_Length
     (Option : Bytes_Socket_Option;
      Value : Bytes)
     return Boolean;

   ------------------------------
   --  Type-safe Get functions --
   ------------------------------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Integer;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Integer_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return OS_Integer;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_64_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Integer_64;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Unsigned_64_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Unsigned_64;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Boolean_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Boolean;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Boolean_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return OS_Boolean;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Mechanism_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Security_Mechanism;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Events_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Poll_Flags;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Type_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Socket_Type;

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Bytes_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return Bytes
   --  If no error occurs, we guarantee Valid_Length
   with Post =>
     (if Error = Ok
      then Valid_Length (Option, Get'Result)
      else Get'Result'Length = 0);

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : String_Socket_Option;
      Error : out ZMQ.Errno.Error)
     return String
   --  If no error occurs, we guarantee Valid_Length
   with Post =>
     (if Error = Ok
      then Valid_Length (Option, Get'Result)
      else Get'Result'Length = 0);

   ------------------------------
   --  Type-safe Set functions --
   ------------------------------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_Socket_Option;
      Value : Integer)
     return Error;

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Integer_Socket_Option;
      Value : OS_Integer)
     return Error;

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_64_Socket_Option;
      Value : Integer_64)
     return Error;

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Unsigned_64_Socket_Option;
      Value : Unsigned_64)
     return Error;

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Boolean_Socket_Option;
      Value : Boolean)
     return Error;

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Boolean_Socket_Option;
      Value : OS_Boolean)
     return Error;

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Bytes_Socket_Option;
      Value : Bytes)
     return Error
   with Pre => Valid_Length (Option, Value);

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : String_Socket_Option;
      Value : String)
     return Error
   with Pre => Valid_Length (Option, Value);

end ZMQ.Sockets;
