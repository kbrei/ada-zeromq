--    Copyright (c) 2007-2015 Contributors as noted in the AUTHORS file
--    This file is part of 0MQ.
--    0MQ is free software; you can redistribute it and/or modify it under
--    the terms of the GNU Lesser General Public License as published by
--    the Free Software Foundation; either version 3 of the License, or
--    (at your option) any later version.
--    0MQ is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU Lesser General Public License for more details.
--    You should have received a copy of the GNU Lesser General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--    *************************************************************************
--    NOTE to contributors. This file comprises the principal public contract
--    for ZeroMQ API users (along with zmq_utils.h). Any change to this file
--    supplied in a stable release SHOULD not break existing applications.
--    In practice this means that the value of constants must not change, and
--    that old values may not be reused for new constants.
--    *************************************************************************

pragma Style_Checks (Off);

with Interfaces; use Interfaces;
with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with System;

package ZMQ.Raw is

  --  Run-time API version detection                                             
   procedure zmq_version
     (major : out int;
      minor : out int;
      patch : out int);  -- /usr/include/zmq.h:162
   pragma Import (C, zmq_version, "zmq_version");

  --**************************************************************************** 
  --  0MQ infrastructure (a.k.a. context) initialisation & termination.          
  --**************************************************************************** 
  --  New API                                                                    
  --  Context options                                                            
  --  Default for new contexts                                                   
   function zmq_ctx_new return System.Address;  -- /usr/include/zmq.h:182
   pragma Import (C, zmq_ctx_new, "zmq_ctx_new");

   function zmq_ctx_term (context : System.Address) return int;  -- /usr/include/zmq.h:183
   pragma Import (C, zmq_ctx_term, "zmq_ctx_term");

   function zmq_ctx_shutdown (ctx_u : System.Address) return int;  -- /usr/include/zmq.h:184
   pragma Import (C, zmq_ctx_shutdown, "zmq_ctx_shutdown");

   function zmq_ctx_set
     (context : System.Address;
      option : int;
      optval : int) return int;  -- /usr/include/zmq.h:185
   pragma Import (C, zmq_ctx_set, "zmq_ctx_set");

   function zmq_ctx_get (context : System.Address; option : int) return int;  -- /usr/include/zmq.h:186
   pragma Import (C, zmq_ctx_get, "zmq_ctx_get");

  --**************************************************************************** 
  --  0MQ message definition.                                                    
  --**************************************************************************** 
   type zmq_msg_t_u_u_array is array (0 .. 63) of aliased unsigned_char;
   type zmq_msg_t is record
      u_u : aliased zmq_msg_t_u_u_array;  -- /usr/include/zmq.h:198
   end record;
   pragma Convention (C_Pass_By_Copy, zmq_msg_t);  -- /usr/include/zmq.h:198

   --  skipped function type zmq_free_fn

   function zmq_msg_init (msg : in out zmq_msg_t) return int;  -- /usr/include/zmq.h:202
   pragma Import (C, zmq_msg_init, "zmq_msg_init");

   function zmq_msg_init_size (msg : in out zmq_msg_t; size : size_t) return int;  -- /usr/include/zmq.h:203
   pragma Import (C, zmq_msg_init_size, "zmq_msg_init_size");

   function zmq_msg_init_data
     (msg : in out zmq_msg_t;
      data : System.Address;
      size : size_t;
      free : not null access procedure (data : System.Address; hint : System.Address);
      hint : System.Address) return int;  -- /usr/include/zmq.h:204
   pragma Import (C, zmq_msg_init_data, "zmq_msg_init_data");

   function zmq_msg_send
     (msg : in out zmq_msg_t;
      s : System.Address;
      flags : int) return int;  -- /usr/include/zmq.h:206
   pragma Import (C, zmq_msg_send, "zmq_msg_send");

   function zmq_msg_recv
     (msg : in out zmq_msg_t;
      s : System.Address;
      flags : int) return int;  -- /usr/include/zmq.h:207
   pragma Import (C, zmq_msg_recv, "zmq_msg_recv");

   function zmq_msg_close (msg : in out zmq_msg_t) return int;  -- /usr/include/zmq.h:208
   pragma Import (C, zmq_msg_close, "zmq_msg_close");

   function zmq_msg_move (dest : in out zmq_msg_t; src : in out zmq_msg_t) return int;  -- /usr/include/zmq.h:209
   pragma Import (C, zmq_msg_move, "zmq_msg_move");

   function zmq_msg_copy (dest : in out zmq_msg_t; src : in out zmq_msg_t) return int;  -- /usr/include/zmq.h:210
   pragma Import (C, zmq_msg_copy, "zmq_msg_copy");

   function zmq_msg_data (msg : in out zmq_msg_t) return System.Address;  -- /usr/include/zmq.h:211
   pragma Import (C, zmq_msg_data, "zmq_msg_data");

   function zmq_msg_size (msg : in out zmq_msg_t) return size_t;  -- /usr/include/zmq.h:212
   pragma Import (C, zmq_msg_size, "zmq_msg_size");

   function zmq_msg_more (msg : in out zmq_msg_t) return int;  -- /usr/include/zmq.h:213

   pragma Import (C, zmq_msg_more, "zmq_msg_more");

   function zmq_msg_get (msg : in out zmq_msg_t; property : int) return int;  -- /usr/include/zmq.h:214
   pragma Import (C, zmq_msg_get, "zmq_msg_get");

   function zmq_msg_set
     (msg : in out zmq_msg_t;
      property : int;
      optval : int) return int;  -- /usr/include/zmq.h:215
   pragma Import (C, zmq_msg_set, "zmq_msg_set");

   function zmq_msg_gets (msg : in out zmq_msg_t; property : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr;  -- /usr/include/zmq.h:216
   pragma Import (C, zmq_msg_gets, "zmq_msg_gets");

  --**************************************************************************** 
  --  0MQ socket definition.                                                     
  --**************************************************************************** 
  --  Socket types.                                                              
  --  Socket options.                                                            
  --  Message options                                                            
  --  Send/recv options.                                                         
  --  Security mechanisms                                                        
  --**************************************************************************** 
  --  0MQ socket events and monitoring                                           
  --**************************************************************************** 
  --  Socket transport events (TCP and IPC only)                                 
   function zmq_socket (context : System.Address; c_type : int) return System.Address;  -- /usr/include/zmq.h:343
   pragma Import (C, zmq_socket, "zmq_socket");

   function zmq_close (s : System.Address) return int;  -- /usr/include/zmq.h:344
   pragma Import (C, zmq_close, "zmq_close");

   function zmq_setsockopt
     (s : System.Address;
      option : int;
      optval : System.Address;
      optvallen : size_t) return int;  -- /usr/include/zmq.h:345
   pragma Import (C, zmq_setsockopt, "zmq_setsockopt");

   function zmq_getsockopt
     (s : System.Address;
      option : int;
      optval : System.Address;
      optvallen : access size_t) return int;  -- /usr/include/zmq.h:347
   pragma Import (C, zmq_getsockopt, "zmq_getsockopt");

   function zmq_bind (s : System.Address; addr : Interfaces.C.Strings.chars_ptr) return int;  -- /usr/include/zmq.h:349
   pragma Import (C, zmq_bind, "zmq_bind");

   function zmq_connect (s : System.Address; addr : Interfaces.C.Strings.chars_ptr) return int;  -- /usr/include/zmq.h:350
   pragma Import (C, zmq_connect, "zmq_connect");

   function zmq_unbind (s : System.Address; addr : Interfaces.C.Strings.chars_ptr) return int;  -- /usr/include/zmq.h:351
   pragma Import (C, zmq_unbind, "zmq_unbind");

   function zmq_disconnect (s : System.Address; addr : Interfaces.C.Strings.chars_ptr) return int;  -- /usr/include/zmq.h:352
   pragma Import (C, zmq_disconnect, "zmq_disconnect");

   function zmq_send
     (s : System.Address;
      buf : System.Address;
      len : size_t;
      flags : int) return int;  -- /usr/include/zmq.h:353
   pragma Import (C, zmq_send, "zmq_send");

   function zmq_send_const
     (s : System.Address;
      buf : System.Address;
      len : size_t;
      flags : int) return int;  -- /usr/include/zmq.h:354
   pragma Import (C, zmq_send_const, "zmq_send_const");

   function zmq_recv
     (s : System.Address;
      buf : System.Address;
      len : size_t;
      flags : int) return int;  -- /usr/include/zmq.h:355
   pragma Import (C, zmq_recv, "zmq_recv");

   function zmq_socket_monitor
     (s : System.Address;
      addr : Interfaces.C.Strings.chars_ptr;
      events : int) return int;  -- /usr/include/zmq.h:356
   pragma Import (C, zmq_socket_monitor, "zmq_socket_monitor");

  --**************************************************************************** 
  --  I/O multiplexing.                                                          
  --**************************************************************************** 
   type zmq_pollitem_t is record
      socket : System.Address;  -- /usr/include/zmq.h:369
      fd : aliased int;  -- /usr/include/zmq.h:373
      events : aliased short;  -- /usr/include/zmq.h:375
      revents : aliased short;  -- /usr/include/zmq.h:376
   end record;
   pragma Convention (C_Pass_By_Copy, zmq_pollitem_t);  -- /usr/include/zmq.h:367

   function zmq_poll
     (items : access zmq_pollitem_t;
      nitems : int;
      timeout : long) return int;  -- /usr/include/zmq.h:381
   pragma Import (C, zmq_poll, "zmq_poll");

  --**************************************************************************** 
  --  Message proxying                                                           
  --**************************************************************************** 
   function zmq_proxy
     (frontend : System.Address;
      backend : System.Address;
      capture : System.Address) return int;  -- /usr/include/zmq.h:387
   pragma Import (C, zmq_proxy, "zmq_proxy");

   function zmq_proxy_steerable
     (frontend : System.Address;
      backend : System.Address;
      capture : System.Address;
      control : System.Address) return int;  -- /usr/include/zmq.h:388
   pragma Import (C, zmq_proxy_steerable, "zmq_proxy_steerable");

  --**************************************************************************** 
  --  Probe library capabilities                                                 
  --**************************************************************************** 
   function zmq_has (capability : Interfaces.C.Strings.chars_ptr) return int;  -- /usr/include/zmq.h:395
   pragma Import (C, zmq_has, "zmq_has");

  --**************************************************************************** 
  --  Encryption functions                                                       
  --**************************************************************************** 
  --  Encode data with Z85 encoding. Returns encoded data                        
   function zmq_z85_encode
     (dest : Interfaces.C.Strings.chars_ptr;
      data : access Unsigned_8;
      size : size_t) return Interfaces.C.Strings.chars_ptr;  -- /usr/include/zmq.h:413
   pragma Import (C, zmq_z85_encode, "zmq_z85_encode");

  --  Decode data with Z85 encoding. Returns decoded data                        
   function zmq_z85_decode (dest : access Unsigned_8; string : Interfaces.C.Strings.chars_ptr) return access Unsigned_8;  -- /usr/include/zmq.h:416
   pragma Import (C, zmq_z85_decode, "zmq_z85_decode");

  --  Generate z85-encoded public and private keypair with libsodium.            
  --  Returns 0 on success.                                                      
   function zmq_curve_keypair (z85_public_key : Interfaces.C.Strings.chars_ptr; z85_secret_key : Interfaces.C.Strings.chars_ptr) return int;  -- /usr/include/zmq.h:420
   pragma Import (C, zmq_curve_keypair, "zmq_curve_keypair");

end ZMQ.Raw;
