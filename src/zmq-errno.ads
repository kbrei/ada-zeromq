with Interfaces.C; use Interfaces.C;

package ZMQ.Errno
is
   type Error is
     (Ok, --  No error
      Unknown, --  Unkown error
      --  POSIX errors
      Argument_List_Too_Long, --  E2BIG
      Permission_Denied, --  EACCES
      Address_Already_In_Use, --  EADDRINUSE
      Address_Not_Available, --  EADDRNOTAVAIL
      Address_Family_Not_Supported, --  EAFNOSUPPORT
      Resource_Temporarily_Unavailable, --  EAGAIN
      Connection_Already_In_Progress, --  EALREADY
      Bad_File_Descriptor, --  EBADF
      Bad_Message, --  EBADMSG
      Device_Or_Resource_Busy, --  EBUSY
      Operation_Canceled, --  ECANCELED
      No_Child_Processes, --  ECHILD
      Connection_Aborted, --  ECONNABORTED
      Connection_Refused, --  ECONNREFUSED
      Connection_Reset, --  ECONNRESET
      Resource_Deadlock_Avoided, --  EDEADLK
      Destination_Address_Required, --  EDESTADDRREQ
      Math_Argument, --  EDOM
      Disk_Quota_Exceeded, --  EDQUOT
      File_Exists, --  EEXIST
      Bad_Address, --  EFAULT
      File_Too_Large, --  EFBIG
      Host_Is_Unreachable, --  EHOSTUNREACH
      Identifier_Removed, --  EIDRM
      Illegal_Byte_Sequence, --  EILSEQ
      Operation_In_Progress, --  EINPROGRESS
      Interrupted_Function_Call, --  EINTR
      Invalid_Argument, --  EINVAL
      Input_Output_Error, --  EIO
      Socket_Is_Connected, --  EISCONN
      Is_A_Directory, --  EISDIR
      Too_Many_Symbolic_Links, --  ELOOP
      Too_Many_Open_Files, --  EMFILE
      Too_Many_Links, --  EMLINK
      Message_Too_Long, --  EMSGSIZE
      Multihop_Attempted, --  EMULTIHOP
      Filename_Too_Long, --  ENAMETOOLONG
      Network_Is_Down, --  ENETDOWN
      Connection_Aborted_By_Network, --  ENETRESET
      Network_Unreachable, --  ENETUNREACH
      Too_Many_Open_Files_In_System, --  ENFILE
      No_Buffer_Space_Available, --  ENOBUFS
      No_Message_In_Queue, --  ENODATA
      No_Such_Device, --  ENODEV
      No_Such_File_Or_Directory, --  ENOENT
      Exec_Format_Error, --  ENOEXEC
      No_Locks_Available, --  ENOLCK
      Link_Has_Been_Severed, --  ENOLINK
      Not_Enough_Space, --  ENOMEM
      Wrong_Message_Type, --  ENOMSG
      Protocol_Not_Available, --  ENOPROTOOPT
      No_Space_Left_On_Device, --  ENOSPC
      No_Stream_Resources, --  ENOSR
      Not_A_Stream, --  ENOSTR
      Function_Not_Implemented, --  ENOSYS
      The_Socket_Is_Not_Connected, --  ENOTCONN
      Not_A_Directory, --  ENOTDIR
      Directory_Not_Empty, --  ENOTEMPTY
      Not_A_Socket, --  ENOTSOCK
      Operation_Not_Supported, --  ENOTSUP
      No_TTY, --  ENOTTY
      No_Such_Device_Or_Address, --  ENXIO
      Operation_Not_Supported_On_Socket, --  EOPNOTSUPP
      Value_Too_Large, --  EOVERFLOW
      Operation_Not_Permitted, --  EPERM
      Broken_Pipe, --  EPIPE
      Protocol_Error, --  EPROTO
      Protocol_Not_Supported, --  EPROTONOSUPPORT
      Protocol_Wrong_Type_For_Socket, --  EPROTOTYPE
      Result_Too_Large, --  ERANGE
      Read_Only_Filesystem, --  EROFS
      Invalid_Seek, --  ESPIPE
      No_Such_Process, --  ESRCH
      Stale_File_Handle, --  ESTALE
      Timer_Expired, --  ETIME
      Connection_Timed_Out, --  ETIMEDOUT
      Text_File_Busy, --  ETXTBSY
      Operation_Would_Block, --  EWOULDBLOCK
      Improper_Link, --  EXDEV

      --  ZMQ specific
      State_Machine, -- EFSM
      Incompatible_Protocol, -- ENOCOMPATPROTO
      Context_Terminated, -- ETERM
      No_IO_Thread_Available); -- EMTHREAD

   Unknown_Error : exception; --  unidentified error

   Argument_List_Too_Long_Error : exception; --  E2BIG
   Permission_Denied_Error : exception; --  EACCES
   Address_Already_In_Use_Error : exception; --  EADDRINUSE
   Address_Not_Available_Error : exception; --  EADDRNOTAVAIL
   Address_Family_Not_Supported_Error : exception; --  EAFNOSUPPORT
   Resource_Temporarily_Unavailable_Error : exception; --  EAGAIN
   Connection_Already_In_Progress_Error : exception; --  EALREADY
   Bad_File_Descriptor_Error : exception; --  EBADF
   Bad_Message_Error : exception; --  EBADMSG
   Device_Or_Resource_Busy_Error : exception; --  EBUSY
   Operation_Canceled_Error : exception; --  ECANCELED
   No_Child_Processes_Error : exception; --  ECHILD
   Connection_Aborted_Error : exception; --  ECONNABORTED
   Connection_Refused_Error : exception; --  ECONNREFUSED
   Connection_Reset_Error : exception; --  ECONNRESET
   Resource_Deadlock_Avoided_Error : exception; --  EDEADLK
   Destination_Address_Required_Error : exception; --  EDESTADDRREQ
   Math_Argument_Error : exception; --  EDOM
   Disk_Quota_Exceeded_Error : exception; --  EDQUOT
   File_Exists_Error : exception; --  EEXIST
   Bad_Address_Error : exception; --  EFAULT
   File_Too_Large_Error : exception; --  EFBIG
   Host_Is_Unreachable_Error : exception; --  EHOSTUNREACH
   Identifier_Removed_Error : exception; --  EIDRM
   Illegal_Byte_Sequence_Error : exception; --  EILSEQ
   Operation_In_Progress_Error : exception; --  EINPROGRESS
   Interrupted_Function_Call_Error : exception; --  EINTR
   Invalid_Argument_Error : exception; --  EINVAL
   Input_Output_Error_Error : exception; --  EIO
   Socket_Is_Connected_Error : exception; --  EISCONN
   Is_A_Directory_Error : exception; --  EISDIR
   Too_Many_Symbolic_Links_Error : exception; --  ELOOP
   Too_Many_Open_Files_Error : exception; --  EMFILE
   Too_Many_Links_Error : exception; --  EMLINK
   Message_Too_Long_Error : exception; --  EMSGSIZE
   Multihop_Attempted_Error : exception; --  EMULTIHOP
   Filename_Too_Long_Error : exception; --  ENAMETOOLONG
   Network_Is_Down_Error : exception; --  ENETDOWN
   Connection_Aborted_By_Network_Error : exception; --  ENETRESET
   Network_Unreachable_Error : exception; --  ENETUNREACH
   Too_Many_Open_Files_In_System_Error : exception; --  ENFILE
   No_Buffer_Space_Available_Error : exception; --  ENOBUFS
   No_Message_In_Queue_Error : exception; --  ENODATA
   No_Such_Device_Error : exception; --  ENODEV
   No_Such_File_Or_Directory_Error : exception; --  ENOENT
   Exec_Format_Error_Error : exception; --  ENOEXEC
   No_Locks_Available_Error : exception; --  ENOLCK
   Link_Has_Been_Severed_Error : exception; --  ENOLINK
   Not_Enough_Space_Error : exception; --  ENOMEM
   Wrong_Message_Type_Error : exception; --  ENOMSG
   Protocol_Not_Available_Error : exception; --  ENOPROTOOPT
   No_Space_Left_On_Device_Error : exception; --  ENOSPC
   No_Stream_Resources_Error : exception; --  ENOSR
   Not_A_Stream_Error : exception; --  ENOSTR
   Function_Not_Implemented_Error : exception; --  ENOSYS
   The_Socket_Is_Not_Connected_Error : exception; --  ENOTCONN
   Not_A_Directory_Error : exception; --  ENOTDIR
   Directory_Not_Empty_Error : exception; --  ENOTEMPTY
   Not_A_Socket_Error : exception; --  ENOTSOCK
   Operation_Not_Supported_Error : exception; --  ENOTSUP
   No_TTY_Error : exception; --  ENOTTY
   No_Such_Device_Or_Address_Error : exception; --  ENXIO
   Operation_Not_Supported_On_Socket_Error : exception; --  EOPNOTSUPP
   Value_Too_Large_Error : exception; --  EOVERFLOW
   Operation_Not_Permitted_Error : exception; --  EPERM
   Broken_Pipe_Error : exception; --  EPIPE
   Protocol_Error_Error : exception; --  EPROTO
   Protocol_Not_Supported_Error : exception; --  EPROTONOSUPPORT
   Protocol_Wrong_Type_For_Socket_Error : exception; --  EPROTOTYPE
   Result_Too_Large_Error : exception; --  ERANGE
   Read_Only_Filesystem_Error : exception; --  EROFS
   Invalid_Seek_Error : exception; --  ESPIPE
   No_Such_Process_Error : exception; --  ESRCH
   Stale_File_Handle_Error : exception; --  ESTALE
   Timer_Expired_Error : exception; --  ETIME
   Connection_Timed_Out_Error : exception; --  ETIMEDOUT
   Text_File_Busy_Error : exception; --  ETXTBSY
   Operation_Would_Block_Error : exception; --  EWOULDBLOCK
   Improper_Link_Error : exception; --  EXDEV

   State_Machine_Error : exception; -- EFSM
   Incompatible_Protocol_Error : exception; -- ENOCOMPATPROTO
   Context_Terminated_Error : exception; -- ETERM
   No_IO_Thread_Available_Error : exception; -- EMTHREAD

   function Check
     (Return_Value : int;
      Bad_Range_Start : int := -1;
      Bad_Range_End : int := -1)
     return Error;

   procedure Raise_Error
     (Error_Kind : Error;
      Message : String := "");
end ZMQ.Errno;
