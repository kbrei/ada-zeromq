with Interfaces.C; use Interfaces.C;
with System;
with ZMQ.Errno; use ZMQ.Errno;

package ZMQ.Contexts is
   type Context is private;

   type Context_Option is
     (IO_Threads,
      Max_Sockets,
      Thread_Priority,
      Thread_Sched_Policy)
   with Convention => C;

   for Context_Option'Size use int'Size;
   for Context_Option'Alignment use int'Alignment;
   for Context_Option use
     (IO_Threads => 1,
      Max_Sockets => 2,
      --  TODO: this maps to the same value as ZMQ_SOCKET_LIMIT
      Thread_Priority => 3,
      Thread_Sched_Policy => 4);

   type Context_Value is new Integer;

   function Create return Context;

   function Term
     (Context : ZMQ.Contexts.Context)
     return Error;

   function Shutdown
     (Context : ZMQ.Contexts.Context)
     return Error;

   function Set
     (Context : ZMQ.Contexts.Context;
      Option : Context_Option;
      Value : Context_Value)
     return Error;

   function Get
     (Context : ZMQ.Contexts.Context;
      Option : Context_Option;
      Error : out ZMQ.Errno.Error)
     return Context_Value;

private
   type Context is new System.Address;

end ZMQ.Contexts;
