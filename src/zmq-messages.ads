with Interfaces.C; use Interfaces.C;
with System;

with ZMQ.Flags; use ZMQ.Flags;
with ZMQ.Sockets; use ZMQ.Sockets;
with ZMQ.Errno; use ZMQ.Errno;
private with ZMQ.Raw;

package ZMQ.Messages is
   type Message is private;

   type Message_Option is
     (More,
      Source_File_Descriptor,
      Shared)
   with Convention => C;
   for Message_Option'Size use int'Size;
   for Message_Option'Alignment use int'Alignment;
   for Message_Option use
     (More => 1,
      Source_File_Descriptor => 2,
      Shared => 3);

   type Boolean_Message_Option is new Message_Option
   with Static_Predicate => Boolean_Message_Option in
      More |
      Shared;

   --  We should be able to get away with this in windows
   --  as it's an unsigned there
   type OS_Socket is new int;

   type OS_Socket_Message_Option is new Message_Option
   with Static_Predicate => OS_Socket_Message_Option in
      Source_File_Descriptor;

   type Free_Callback is access procedure
     (Data : System.Address;
      Hint : System.Address)
   with Convention => C;

   function Create return Message;

   function Create
      (Size : ZMQ.Size)
     return Message;

   function Create
     (Data : not null access Bytes;
      Clean_Up : Free_Callback := null;
      Hint : System.Address := System.Null_Address)
     return Message;

   function Move
     (To : in out Message;
      From : in out Message)
     return Error;

   function Copy
     (To : in out Message;
      From : in out Message)
     return Error;

   function More
     (Message : in out ZMQ.Messages.Message)
     return Boolean;

   --  Caution, this copies the whole content!
   function Data
     (Message : in out ZMQ.Messages.Message)
     return Bytes;

   --  The only possible error is EINVAL
   --  This this should be impossible
   function Get
     (Message : in out ZMQ.Messages.Message;
      Option : Boolean_Message_Option)
     return Boolean;

   function Get
     (Message : in out ZMQ.Messages.Message;
      Option : OS_Socket_Message_Option)
     return OS_Socket;

   procedure Set
     (Message : in out ZMQ.Messages.Message;
      Option : Boolean_Message_Option;
      Value : Boolean);

   procedure Set
     (Message : in out ZMQ.Messages.Message;
      Option : OS_Socket_Message_Option;
      Value : OS_Socket);

   function Send
     (Socket : ZMQ.Sockets.Socket;
      Message : in out ZMQ.Messages.Message;
      Flags : Send_Recv_Flags;
      Bytes_Send : out Integer)
     return Error;

   function Receive
     (Socket : ZMQ.Sockets.Socket;
      Message : in out ZMQ.Messages.Message;
      Flags : Send_Recv_Flags;
      Bytes_Received : out Integer)
     return Error;

private
   type Message is new ZMQ.Raw.zmq_msg_t;

end ZMQ.Messages;
