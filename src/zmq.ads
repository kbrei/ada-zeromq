with Interfaces;
with Interfaces.C;

package ZMQ is

   type Unsigned is mod 2 ** Integer'Size;
   subtype Unsigned_Positive is Unsigned range 1 .. Unsigned'Last;

   type Byte is new Interfaces.Unsigned_8;
   type Bytes is array (Unsigned_Positive range <>) of aliased Byte;

   type Size is new Interfaces.C.size_t;

   type Integer_64 is range -2 ** 63 .. 2 ** 63 - 1;
   for Integer_64'Size use 64;

   type Unsigned_64 is new Interfaces.Unsigned_64;
end ZMQ;
