with Generic_Flags;
with Interfaces.C; use Interfaces.C;

package ZMQ.Flags is
   type int_mod is mod 2**(int'Size);

   package Send_Recv is new Generic_Flags (int_mod);
   package Event is new Generic_Flags (int_mod);
   package Poll is new Generic_Flags (int_mod);

   subtype Send_Recv_Flags is Send_Recv.Flags_Type;
   subtype Event_Flags is Event.Flags_Type;
   subtype Poll_Flags is Poll.Flags_Type;

   --  Send/Recv flags
   Dont_Wait : constant Send_Recv_Flags
     with Import, Convention => C, Link_Name => "zmq_dontwait";

   Send_More : constant Send_Recv_Flags
     with Import, Convention => C, Link_Name => "zmq_sndmore";

   No_Send_Recv_Flags : constant Send_Recv_Flags
     with Import, Convention => C, Link_Name => "zero";

   --  Event flags
   Connected : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_connected";

   Connect_Delayed : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_connect_delayed";

   Connect_Retried : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_connect_retried";

   Listening : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_listening";

   Bind_Failed : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_bind_failed";

   Accepted : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_accepted";

   Accept_Failed : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_accept_failed";

   Closed : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_closed";

   Close_failed : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_close_failed";

   Disconnected : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_disconnected";

   Monitor_stopped : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_monitor_stopped";

   All_Event_Flags : constant Event_Flags
     with Import, Convention => C, Link_Name => "zmq_event_all";

   No_Event_Flags : constant Event_Flags
     with Import, Convention => C, Link_Name => "zero";

   --  Poll flags
   Poll_In : constant Poll_Flags
     with Import, Convention => C, Link_Name => "zmq_pollin";

   Poll_Out : constant Poll_Flags
     with Import, Convention => C, Link_Name => "zmq_pollout";

   Poll_Error : constant Poll_Flags
     with Import, Convention => C, Link_Name => "zmq_pollerr";

   No_Poll_Flags : constant Poll_Flags
     with Import, Convention => C, Link_Name => "zero";

   --  Convenience re-exports
   --  Send_Recv
   function Add
     (To : Send_Recv_Flags;
      To_Add : Send_Recv_Flags)
     return Send_Recv_Flags
     renames Send_Recv.Add;
   function "+"
     (Left : Send_Recv_Flags;
      Right : Send_Recv_Flags)
     return Send_Recv_Flags
     renames Add;

   function Remove
     (From : Send_Recv_Flags;
      To_Remove : Send_Recv_Flags)
     return Send_Recv_Flags
     renames Send_Recv.Remove;
   function "-"
     (Left : Send_Recv_Flags;
      Right : Send_Recv_Flags)
     return Send_Recv_Flags
     renames Remove;

   function Toggle
     (Inside : Send_Recv_Flags;
      To_Toggle : Send_Recv_Flags)
     return Send_Recv_Flags
     renames Send_Recv.Toggle;

   function Contains
     (Set : Send_Recv_Flags;
      Subset : Send_Recv_Flags)
     return Boolean
     renames Send_Recv.Contains;
   function ">"
     (Left : Send_Recv_Flags;
      Right : Send_Recv_Flags)
     return Boolean
     renames Contains;

   function Equal
     (Left : Send_Recv_Flags;
      Right : Send_Recv_Flags)
     return Boolean
     renames Send_Recv.Equal;
   function "="
     (Left : Send_Recv_Flags;
      Right : Send_Recv_Flags)
     return Boolean
     renames Equal;

   --  Event
   function Add
     (To : Event_Flags;
      To_Add : Event_Flags)
     return Event_Flags
     renames Event.Add;
   function "+"
     (Left : Event_Flags;
      Right : Event_Flags)
     return Event_Flags
     renames Add;

   function Remove
     (From : Event_Flags;
      To_Remove : Event_Flags)
     return Event_Flags
     renames Event.Remove;
   function "-"
     (Left : Event_Flags;
      Right : Event_Flags)
     return Event_Flags
     renames Remove;

   function Toggle
     (Inside : Event_Flags;
      To_Toggle : Event_Flags)
     return Event_Flags
     renames Event.Toggle;

   function Contains
     (Set : Event_Flags;
      Subset : Event_Flags)
     return Boolean
     renames Event.Contains;
   function ">"
     (Left : Event_Flags;
      Right : Event_Flags)
     return Boolean
     renames Contains;

   function Equal
     (Left : Event_Flags;
      Right : Event_Flags)
     return Boolean
     renames Event.Equal;
   function "="
     (Left : Event_Flags;
      Right : Event_Flags)
     return Boolean
     renames Equal;

   --  Poll
   function Add
     (To : Poll_Flags;
      To_Add : Poll_Flags)
     return Poll_Flags
     renames Poll.Add;
   function "+"
     (Left : Poll_Flags;
      Right : Poll_Flags)
     return Poll_Flags
     renames Add;

   function Remove
     (From : Poll_Flags;
      To_Remove : Poll_Flags)
     return Poll_Flags
     renames Poll.Remove;
   function "-"
     (Left : Poll_Flags;
      Right : Poll_Flags)
     return Poll_Flags
     renames Remove;

   function Toggle
     (Inside : Poll_Flags;
      To_Toggle : Poll_Flags)
     return Poll_Flags
     renames Poll.Toggle;

   function Contains
     (Set : Poll_Flags;
      Subset : Poll_Flags)
     return Boolean
     renames Poll.Contains;
   function ">"
     (Left : Poll_Flags;
      Right : Poll_Flags)
     return Boolean
     renames Contains;

   function Equal
     (Left : Poll_Flags;
      Right : Poll_Flags)
     return Boolean
     renames Poll.Equal;
   function "="
     (Left : Poll_Flags;
      Right : Poll_Flags)
     return Boolean
     renames Equal;

end ZMQ.Flags;
