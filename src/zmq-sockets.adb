package body ZMQ.Sockets is

   ------------
   -- Create --
   ------------

   function Create
     (Context : ZMQ.Contexts.Context;
      Sock_Type : Socket_Type)
      return Socket
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Create unimplemented");
      raise Program_Error with "Unimplemented function Create";
      return Create (Context, Sock_Type);
   end Create;

   -----------
   -- Close --
   -----------

   function Close
     (Socket : ZMQ.Sockets.Socket)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Close unimplemented");
      raise Program_Error with "Unimplemented function Close";
      return Close (Socket);
   end Close;

   ----------
   -- Bind --
   ----------

   function Bind
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Bind unimplemented");
      raise Program_Error with "Unimplemented function Bind";
      return Bind (Socket, Address);
   end Bind;

   ------------
   -- Unbind --
   ------------

   function Unbind
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Unbind unimplemented");
      raise Program_Error with "Unimplemented function Unbind";
      return Unbind (Socket, Address);
   end Unbind;

   -------------
   -- Connect --
   -------------

   function Connect
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Connect unimplemented");
      raise Program_Error with "Unimplemented function Connect";
      return Connect (Socket, Address);
   end Connect;

   ----------------
   -- Disconnect --
   ----------------

   function Disconnect
     (Socket : ZMQ.Sockets.Socket;
      Address : ZMQ.Sockets.Address)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Disconnect unimplemented");
      raise Program_Error with "Unimplemented function Disconnect";
      return Disconnect (Socket, Address);
   end Disconnect;

   ----------
   -- Send --
   ----------

   function Send
     (Socket : ZMQ.Sockets.Socket;
      Data : not null access constant Bytes;
      Bytes_Send : out Integer;
      Flags : Send_Recv_Flags := No_Send_Recv_Flags)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Send unimplemented");
      raise Program_Error with "Unimplemented function Send";
      return Send (Socket, Data, Bytes_Send, Flags);
   end Send;

   ----------------
   -- Send_Const --
   ----------------

   function Send_Const
     (Socket : ZMQ.Sockets.Socket;
      Data : not null access constant Bytes;
      Flags : Send_Recv_Flags := No_Send_Recv_Flags;
      Bytes_Send : out Integer)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Send_Const unimplemented");
      raise Program_Error with "Unimplemented function Send_Const";
      return Send_Const (Socket, Data, Flags, Bytes_Send);
   end Send_Const;

   -------------
   -- Receive --
   -------------

   function Receive
     (Socket : ZMQ.Sockets.Socket;
      Data : not null access Bytes;
      Maximum_Bytes : Size;
      Bytes_Received : out Integer;
      Flags : Send_Recv_Flags := No_Send_Recv_Flags)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Receive unimplemented");
      raise Program_Error with "Unimplemented function Receive";
      return Receive (Socket, Data, Maximum_Bytes, Bytes_Received, Flags);
   end Receive;

   ------------------
   -- Valid_Length --
   ------------------

   function Valid_Length
     (Option : String_Socket_Option;
      Value : String)
      return Boolean
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Valid_Length unimplemented");
      raise Program_Error with "Unimplemented function Valid_Length";
      return Valid_Length (Option, Value);
   end Valid_Length;

   ------------------
   -- Valid_Length --
   ------------------

   function Valid_Length
     (Option : Bytes_Socket_Option;
      Value : Bytes)
      return Boolean
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Valid_Length unimplemented");
      raise Program_Error with "Unimplemented function Valid_Length";
      return Valid_Length (Option, Value);
   end Valid_Length;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Integer
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Integer_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return OS_Integer
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_64_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Integer_64
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Unsigned_64_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Unsigned_64
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Boolean_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Boolean
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Boolean_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return OS_Boolean
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Mechanism_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Security_Mechanism
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Events_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Poll_Flags
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Type_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Socket_Type
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : Bytes_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return Bytes
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Get --
   ---------

   function Get
     (Socket : ZMQ.Sockets.Socket;
      Option : String_Socket_Option;
      Error : out ZMQ.Errno.Error)
      return String
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Get unimplemented");
      raise Program_Error with "Unimplemented function Get";
      return Get (Socket, Option, Error);
   end Get;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_Socket_Option;
      Value : Integer)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Integer_Socket_Option;
      Value : OS_Integer)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Integer_64_Socket_Option;
      Value : Integer_64)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Unsigned_64_Socket_Option;
      Value : Unsigned_64)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Boolean_Socket_Option;
      Value : Boolean)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : OS_Boolean_Socket_Option;
      Value : OS_Boolean)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : Bytes_Socket_Option;
      Value : Bytes)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

   ---------
   -- Set --
   ---------

   function Set
     (Socket : ZMQ.Sockets.Socket;
      Option : String_Socket_Option;
      Value : String)
      return Error
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set unimplemented");
      raise Program_Error with "Unimplemented function Set";
      return Set (Socket, Option, Value);
   end Set;

end ZMQ.Sockets;
