package body ZMQ.Errno
is
   E2BIG : constant int with
     Import, Convention => C, Link_Name => "e2big";
   EACCES : constant int with
     Import, Convention => C, Link_Name => "eacces";
   EADDRINUSE : constant int with
     Import, Convention => C, Link_Name => "eaddrinuse";
   EADDRNOTAVAIL : constant int with
     Import, Convention => C, Link_Name => "eaddrnotavail";
   EAFNOSUPPORT : constant int with
     Import, Convention => C, Link_Name => "eafnosupport";
   EAGAIN : constant int with
     Import, Convention => C, Link_Name => "eagain";
   EALREADY : constant int with
     Import, Convention => C, Link_Name => "ealready";
   EBADF : constant int with
     Import, Convention => C, Link_Name => "ebadf";
   EBADMSG : constant int with
     Import, Convention => C, Link_Name => "ebadmsg";
   EBUSY : constant int with
     Import, Convention => C, Link_Name => "ebusy";
   ECANCELED : constant int with
     Import, Convention => C, Link_Name => "ecanceled";
   ECHILD : constant int with
     Import, Convention => C, Link_Name => "echild";
   ECONNABORTED : constant int with
     Import, Convention => C, Link_Name => "econnaborted";
   ECONNREFUSED : constant int with
     Import, Convention => C, Link_Name => "econnrefused";
   ECONNRESET : constant int with
     Import, Convention => C, Link_Name => "econnreset";
   EDEADLK : constant int with
     Import, Convention => C, Link_Name => "edeadlk";
   EDESTADDRREQ : constant int with
     Import, Convention => C, Link_Name => "edestaddrreq";
   EDOM : constant int with
     Import, Convention => C, Link_Name => "edom";
   EDQUOT : constant int with
     Import, Convention => C, Link_Name => "edquot";
   EEXIST : constant int with
     Import, Convention => C, Link_Name => "eexist";
   EFAULT : constant int with
     Import, Convention => C, Link_Name => "efault";
   EFBIG : constant int with
     Import, Convention => C, Link_Name => "efbig";
   EHOSTUNREACH : constant int with
     Import, Convention => C, Link_Name => "ehostunreach";
   EIDRM : constant int with
     Import, Convention => C, Link_Name => "eidrm";
   EILSEQ : constant int with
     Import, Convention => C, Link_Name => "eilseq";
   EINPROGRESS : constant int with
     Import, Convention => C, Link_Name => "einprogress";
   EINTR : constant int with
     Import, Convention => C, Link_Name => "eintr";
   EINVAL : constant int with
     Import, Convention => C, Link_Name => "einval";
   EIO : constant int with
     Import, Convention => C, Link_Name => "eio";
   EISCONN : constant int with
     Import, Convention => C, Link_Name => "eisconn";
   EISDIR : constant int with
     Import, Convention => C, Link_Name => "eisdir";
   ELOOP : constant int with
     Import, Convention => C, Link_Name => "eloop";
   EMFILE : constant int with
     Import, Convention => C, Link_Name => "emfile";
   EMLINK : constant int with
     Import, Convention => C, Link_Name => "emlink";
   EMSGSIZE : constant int with
     Import, Convention => C, Link_Name => "emsgsize";
   EMULTIHOP : constant int with
     Import, Convention => C, Link_Name => "emultihop";
   ENAMETOOLONG : constant int with
     Import, Convention => C, Link_Name => "enametoolong";
   ENETDOWN : constant int with
     Import, Convention => C, Link_Name => "enetdown";
   ENETRESET : constant int with
     Import, Convention => C, Link_Name => "enetreset";
   ENETUNREACH : constant int with
     Import, Convention => C, Link_Name => "enetunreach";
   ENFILE : constant int with
     Import, Convention => C, Link_Name => "enfile";
   ENOBUFS : constant int with
     Import, Convention => C, Link_Name => "enobufs";
   ENODATA : constant int with
     Import, Convention => C, Link_Name => "enodata";
   ENODEV : constant int with
     Import, Convention => C, Link_Name => "enodev";
   ENOENT : constant int with
     Import, Convention => C, Link_Name => "enoent";
   ENOEXEC : constant int with
     Import, Convention => C, Link_Name => "enoexec";
   ENOLCK : constant int with
     Import, Convention => C, Link_Name => "enolck";
   ENOLINK : constant int with
     Import, Convention => C, Link_Name => "enolink";
   ENOMEM : constant int with
     Import, Convention => C, Link_Name => "enomem";
   ENOMSG : constant int with
     Import, Convention => C, Link_Name => "enomsg";
   ENOPROTOOPT : constant int with
     Import, Convention => C, Link_Name => "enoprotoopt";
   ENOSPC : constant int with
     Import, Convention => C, Link_Name => "enospc";
   ENOSR : constant int with
     Import, Convention => C, Link_Name => "enosr";
   ENOSTR : constant int with
     Import, Convention => C, Link_Name => "enostr";
   ENOSYS : constant int with
     Import, Convention => C, Link_Name => "enosys";
   ENOTCONN : constant int with
     Import, Convention => C, Link_Name => "enotconn";
   ENOTDIR : constant int with
     Import, Convention => C, Link_Name => "enotdir";
   ENOTEMPTY : constant int with
     Import, Convention => C, Link_Name => "enotempty";
   ENOTSOCK : constant int with
     Import, Convention => C, Link_Name => "enotsock";
   ENOTSUP : constant int with
     Import, Convention => C, Link_Name => "enotsup";
   ENOTTY : constant int with
     Import, Convention => C, Link_Name => "enotty";
   ENXIO : constant int with
     Import, Convention => C, Link_Name => "enxio";
   EOPNOTSUPP : constant int with
     Import, Convention => C, Link_Name => "eopnotsupp";
   EOVERFLOW : constant int with
     Import, Convention => C, Link_Name => "eoverflow";
   EPERM : constant int with
     Import, Convention => C, Link_Name => "eperm";
   EPIPE : constant int with
     Import, Convention => C, Link_Name => "epipe";
   EPROTO : constant int with
     Import, Convention => C, Link_Name => "eproto";
   EPROTONOSUPPORT : constant int with
     Import, Convention => C, Link_Name => "eprotonosupport";
   EPROTOTYPE : constant int with
     Import, Convention => C, Link_Name => "eprototype";
   ERANGE : constant int with
     Import, Convention => C, Link_Name => "erange";
   EROFS : constant int with
     Import, Convention => C, Link_Name => "erofs";
   ESPIPE : constant int with
     Import, Convention => C, Link_Name => "espipe";
   ESRCH : constant int with
     Import, Convention => C, Link_Name => "esrch";
   ESTALE : constant int with
     Import, Convention => C, Link_Name => "estale";
   ETIME : constant int with
     Import, Convention => C, Link_Name => "etime";
   ETIMEDOUT : constant int with
     Import, Convention => C, Link_Name => "etimedout";
   ETXTBSY : constant int with
     Import, Convention => C, Link_Name => "etxtbsy";
   EWOULDBLOCK : constant int with
     Import, Convention => C, Link_Name => "ewouldblock";
   EXDEV : constant int with
     Import, Convention => C, Link_Name => "exdev";

   EFSM : constant int with
     Import, Convention => C, Link_Name => "efsm";
   ENOCOMPATPROTO : constant int with
     Import, Convention => C, Link_Name => "enocompatproto";
   ETERM : constant int with
     Import, Convention => C, Link_Name => "eterm";
   EMTHREAD : constant int with
     Import, Convention => C, Link_Name => "emthread";

   Error_Map : constant array (Error) of int :=
     (Ok => 0,
      Unknown => 0,
      Argument_List_Too_Long => E2BIG,
      Permission_Denied => EACCES,
      Address_Already_In_Use => EADDRINUSE,
      Address_Not_Available => EADDRNOTAVAIL,
      Address_Family_Not_Supported => EAFNOSUPPORT,
      Resource_Temporarily_Unavailable => EAGAIN,
      Connection_Already_In_Progress => EALREADY,
      Bad_File_Descriptor => EBADF,
      Bad_Message => EBADMSG,
      Device_Or_Resource_Busy => EBUSY,
      Operation_Canceled => ECANCELED,
      No_Child_Processes => ECHILD,
      Connection_Aborted => ECONNABORTED,
      Connection_Refused => ECONNREFUSED,
      Connection_Reset => ECONNRESET,
      Resource_Deadlock_Avoided => EDEADLK,
      Destination_Address_Required => EDESTADDRREQ,
      Math_Argument => EDOM,
      Disk_Quota_Exceeded => EDQUOT,
      File_Exists => EEXIST,
      Bad_Address => EFAULT,
      File_Too_Large => EFBIG,
      Host_Is_Unreachable => EHOSTUNREACH,
      Identifier_Removed => EIDRM,
      Illegal_Byte_Sequence => EILSEQ,
      Operation_In_Progress => EINPROGRESS,
      Interrupted_Function_Call => EINTR,
      Invalid_Argument => EINVAL,
      Input_Output_Error => EIO,
      Socket_Is_Connected => EISCONN,
      Is_A_Directory => EISDIR,
      Too_Many_Symbolic_Links => ELOOP,
      Too_Many_Open_Files => EMFILE,
      Too_Many_Links => EMLINK,
      Message_Too_Long => EMSGSIZE,
      Multihop_Attempted => EMULTIHOP,
      Filename_Too_Long => ENAMETOOLONG,
      Network_Is_Down => ENETDOWN,
      Connection_Aborted_By_Network => ENETRESET,
      Network_Unreachable => ENETUNREACH,
      Too_Many_Open_Files_In_System => ENFILE,
      No_Buffer_Space_Available => ENOBUFS,
      No_Message_In_Queue => ENODATA,
      No_Such_Device => ENODEV,
      No_Such_File_Or_Directory => ENOENT,
      Exec_Format_Error => ENOEXEC,
      No_Locks_Available => ENOLCK,
      Link_Has_Been_Severed => ENOLINK,
      Not_Enough_Space => ENOMEM,
      Wrong_Message_Type => ENOMSG,
      Protocol_Not_Available => ENOPROTOOPT,
      No_Space_Left_On_Device => ENOSPC,
      No_Stream_Resources => ENOSR,
      Not_A_Stream => ENOSTR,
      Function_Not_Implemented => ENOSYS,
      The_Socket_Is_Not_Connected => ENOTCONN,
      Not_A_Directory => ENOTDIR,
      Directory_Not_Empty => ENOTEMPTY,
      Not_A_Socket => ENOTSOCK,
      Operation_Not_Supported => ENOTSUP,
      No_TTY => ENOTTY,
      No_Such_Device_Or_Address => ENXIO,
      Operation_Not_Supported_On_Socket => EOPNOTSUPP,
      Value_Too_Large => EOVERFLOW,
      Operation_Not_Permitted => EPERM,
      Broken_Pipe => EPIPE,
      Protocol_Error => EPROTO,
      Protocol_Not_Supported => EPROTONOSUPPORT,
      Protocol_Wrong_Type_For_Socket => EPROTOTYPE,
      Result_Too_Large => ERANGE,
      Read_Only_Filesystem => EROFS,
      Invalid_Seek => ESPIPE,
      No_Such_Process => ESRCH,
      Stale_File_Handle => ESTALE,
      Timer_Expired => ETIME,
      Connection_Timed_Out => ETIMEDOUT,
      Text_File_Busy => ETXTBSY,
      Operation_Would_Block => EWOULDBLOCK,
      Improper_Link => EXDEV,

      State_Machine => EFSM,
      Incompatible_Protocol => ENOCOMPATPROTO,
      Context_Terminated => ETERM,
      No_IO_Thread_Available => EMTHREAD);

   function Check
     (Return_Value : int;
      Bad_Range_Start : int := -1;
      Bad_Range_End : int := -1)
     return Error
   is
      function Get_Errno return int
      with Import, Convention => C, Link_Name => "zmq_errno";

      In_Bad_Range : constant Boolean :=
        Return_Value >= Bad_Range_Start or else
        Return_Value <= Bad_Range_End;

      Errno : int := 0;
   begin
      if not In_Bad_Range then
         return Ok;
      end if;

      Errno := Get_Errno;

      --  will return Ok, when errno is 0
      Find : for Ada_Error in Error'Range loop
         if Errno = Error_Map (Ada_Error) then
            return Ada_Error;
         end if;
      end loop Find;

      --  We have some unknown errno
      return Unknown;
   end Check;

   --  semi-auto generated stuff
   pragma Style_Checks (Off);
   procedure Raise_Error
     (Error_Kind : Error;
      Message : String := "")
   is
   begin
      case Error_Kind is
         when Ok => null;
         when Unknown => raise Unknown_Error with "Unknown_Error: " & Message;

         --  Posix errors
         when Argument_List_Too_Long => raise Argument_List_Too_Long_Error with "Argument_List_Too_Long_Error: " & Message;
         when Permission_Denied => raise Permission_Denied_Error with "Permission_Denied_Error: " & Message;
         when Address_Already_In_Use => raise Address_Already_In_Use_Error with "Address_Already_In_Use_Error: " & Message;
         when Address_Not_Available => raise Address_Not_Available_Error with "Address_Not_Available_Error: " & Message;
         when Address_Family_Not_Supported => raise Address_Family_Not_Supported_Error with "Address_Family_Not_Supported_Error: " & Message;
         when Resource_Temporarily_Unavailable => raise Resource_Temporarily_Unavailable_Error with "Resource_Temporarily_Unavailable_Error: " & Message;
         when Connection_Already_In_Progress => raise Connection_Already_In_Progress_Error with "Connection_Already_In_Progress_Error: " & Message;
         when Bad_File_Descriptor => raise Bad_File_Descriptor_Error with "Bad_File_Descriptor_Error: " & Message;
         when Bad_Message => raise Bad_Message_Error with "Bad_Message_Error: " & Message;
         when Device_Or_Resource_Busy => raise Device_Or_Resource_Busy_Error with "Device_Or_Resource_Busy_Error: " & Message;
         when Operation_Canceled => raise Operation_Canceled_Error with "Operation_Canceled_Error: " & Message;
         when No_Child_Processes => raise No_Child_Processes_Error with "No_Child_Processes_Error: " & Message;
         when Connection_Aborted => raise Connection_Aborted_Error with "Connection_Aborted_Error: " & Message;
         when Connection_Refused => raise Connection_Refused_Error with "Connection_Refused_Error: " & Message;
         when Connection_Reset => raise Connection_Reset_Error with "Connection_Reset_Error: " & Message;
         when Resource_Deadlock_Avoided => raise Resource_Deadlock_Avoided_Error with "Resource_Deadlock_Avoided_Error: " & Message;
         when Destination_Address_Required => raise Destination_Address_Required_Error with "Destination_Address_Required_Error: " & Message;
         when Math_Argument => raise Math_Argument_Error with "Math_Argument_Error: " & Message;
         when Disk_Quota_Exceeded => raise Disk_Quota_Exceeded_Error with "Disk_Quota_Exceeded_Error: " & Message;
         when File_Exists => raise File_Exists_Error with "File_Exists_Error: " & Message;
         when Bad_Address => raise Bad_Address_Error with "Bad_Address_Error: " & Message;
         when File_Too_Large => raise File_Too_Large_Error with "File_Too_Large_Error: " & Message;
         when Host_Is_Unreachable => raise Host_Is_Unreachable_Error with "Host_Is_Unreachable_Error: " & Message;
         when Identifier_Removed => raise Identifier_Removed_Error with "Identifier_Removed_Error: " & Message;
         when Illegal_Byte_Sequence => raise Illegal_Byte_Sequence_Error with "Illegal_Byte_Sequence_Error: " & Message;
         when Operation_In_Progress => raise Operation_In_Progress_Error with "Operation_In_Progress_Error: " & Message;
         when Interrupted_Function_Call => raise Interrupted_Function_Call_Error with "Interrupted_Function_Call_Error: " & Message;
         when Invalid_Argument => raise Invalid_Argument_Error with "Invalid_Argument_Error: " & Message;
         when Input_Output_Error => raise Input_Output_Error_Error with "Input/output_Error_Error: " & Message;
         when Socket_Is_Connected => raise Socket_Is_Connected_Error with "Socket_Is_Connected_Error: " & Message;
         when Is_A_Directory => raise Is_A_Directory_Error with "Is_A_Directory_Error: " & Message;
         when Too_Many_Symbolic_Links => raise Too_Many_Symbolic_Links_Error with "Too_Many_Symbolic_Links_Error: " & Message;
         when Too_Many_Open_Files => raise Too_Many_Open_Files_Error with "Too_Many_Open_Files_Error: " & Message;
         when Too_Many_Links => raise Too_Many_Links_Error with "Too_Many_Links_Error: " & Message;
         when Message_Too_Long => raise Message_Too_Long_Error with "Message_Too_Long_Error: " & Message;
         when Multihop_Attempted => raise Multihop_Attempted_Error with "Multihop_Attempted_Error: " & Message;
         when Filename_Too_Long => raise Filename_Too_Long_Error with "Filename_Too_Long_Error: " & Message;
         when Network_Is_Down => raise Network_Is_Down_Error with "Network_Is_Down_Error: " & Message;
         when Connection_Aborted_By_Network => raise Connection_Aborted_By_Network_Error with "Connection_Aborted_By_Network_Error: " & Message;
         when Network_Unreachable => raise Network_Unreachable_Error with "Network_Unreachable_Error: " & Message;
         when Too_Many_Open_Files_In_System => raise Too_Many_Open_Files_In_System_Error with "Too_Many_Open_Files_In_System_Error: " & Message;
         when No_Buffer_Space_Available => raise No_Buffer_Space_Available_Error with "No_Buffer_Space_Available_Error: " & Message;
         when No_Message_In_Queue => raise No_Message_In_Queue_Error with "No_Message_In_Queue_Error: " & Message;
         when No_Such_Device => raise No_Such_Device_Error with "No_Such_Device_Error: " & Message;
         when No_Such_File_Or_Directory => raise No_Such_File_Or_Directory_Error with "No_Such_File_Or_Directory_Error: " & Message;
         when Exec_Format_Error => raise Exec_Format_Error_Error with "Exec_Format_Error_Error: " & Message;
         when No_Locks_Available => raise No_Locks_Available_Error with "No_Locks_Available_Error: " & Message;
         when Link_Has_Been_Severed => raise Link_Has_Been_Severed_Error with "Link_Has_Been_Severed_Error: " & Message;
         when Not_Enough_Space => raise Not_Enough_Space_Error with "Not_Enough_Space_Error: " & Message;
         when Wrong_Message_Type => raise Wrong_Message_Type_Error with "Wrong_Message_Type_Error: " & Message;
         when Protocol_Not_Available => raise Protocol_Not_Available_Error with "Protocol_Not_Available_Error: " & Message;
         when No_Space_Left_On_Device => raise No_Space_Left_On_Device_Error with "No_Space_Left_On_Device_Error: " & Message;
         when No_Stream_Resources => raise No_Stream_Resources_Error with "No_Stream_Resources_Error: " & Message;
         when Not_A_Stream => raise Not_A_Stream_Error with "Not_A_Stream_Error: " & Message;
         when Function_Not_Implemented => raise Function_Not_Implemented_Error with "Function_Not_Implemented_Error: " & Message;
         when The_Socket_Is_Not_Connected => raise The_Socket_Is_Not_Connected_Error with "The_Socket_Is_Not_Connected_Error: " & Message;
         when Not_A_Socket => raise Not_A_Socket_Error with "Not_A_Socket_Error: " & Message;
         when Directory_Not_Empty => raise Directory_Not_Empty_Error with "Directory_Not_Empty_Error: " & Message;
         when Not_A_Directory => raise Not_A_Directory_Error with "Not_A_Directory_Error: " & Message;
         when Operation_Not_Supported => raise Operation_Not_Supported_Error with "Operation_Not_Supported_Error: " & Message;
         when No_TTY => raise No_TTY_Error with "No_TTY: " & Message;
         when No_Such_Device_Or_Address => raise No_Such_Device_Or_Address_Error with "No_Such_Device_Or_Address_Error: " & Message;
         when Operation_Not_Supported_On_Socket => raise Operation_Not_Supported_On_Socket_Error with "Operation_Not_Supported_On_Socket_Error: " & Message;
         when Value_Too_Large => raise Value_Too_Large_Error with "Value_Too_Large_Error: " & Message;
         when Operation_Not_Permitted => raise Operation_Not_Permitted_Error with "Operation_Not_Permitted_Error: " & Message;
         when Broken_Pipe => raise Broken_Pipe_Error with "Broken_Pipe_Error: " & Message;
         when Protocol_Error => raise Protocol_Error_Error with "Protocol_Error_Error: " & Message;
         when Protocol_Not_Supported => raise Protocol_Not_Supported_Error with "Protocol_Not_Supported_Error: " & Message;
         when Protocol_Wrong_Type_For_Socket => raise Protocol_Wrong_Type_For_Socket_Error with "Protocol_Wrong_Type_For_Socket_Error: " & Message;
         when Result_Too_Large => raise Result_Too_Large_Error with "Result_Too_Large_Error: " & Message;
         when Read_Only_Filesystem => raise Read_Only_Filesystem_Error with "Read_Only_Filesystem_Error: " & Message;
         when Invalid_Seek => raise Invalid_Seek_Error with "Invalid_Seek_Error: " & Message;
         when No_Such_Process => raise No_Such_Process_Error with "No_Such_Process_Error: " & Message;
         when Stale_File_Handle => raise Stale_File_Handle_Error with "Stale_File_Handle_Error: " & Message;
         when Timer_Expired => raise Timer_Expired_Error with "Timer_Expired_Error: " & Message;
         when Connection_Timed_Out => raise Connection_Timed_Out_Error with "Connection_Timed_Out_Error: " & Message;
         when Text_File_Busy => raise Text_File_Busy_Error with "Text_File_Busy_Error: " & Message;
         when Operation_Would_Block => raise Operation_Would_Block_Error with "Operation_Would_Block_Error: " & Message;
         when Improper_Link => raise Improper_Link_Error with "Improper_Link_Error: " & Message;

         --  zmq errors
         when State_Machine => raise State_Machine_Error with "State_Machine_Error: " & Message;
         when Incompatible_Protocol => raise Incompatible_Protocol_Error with "Incompatible_Protocol_Error: " & Message;
         when Context_Terminated => raise Context_Terminated_Error with "Context_Terminated_Error: " & Message;
         when No_IO_Thread_Available => raise No_IO_Thread_Available_Error with "No_IO_Thread_Available_Error: " & Message;
      end case;
   end Raise_Error;
   pragma Style_Checks (On);

end ZMQ.Errno;
