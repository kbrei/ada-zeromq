#include <errno.h>
#include <zmq.h>

const int zero = 0;

// api version stuff
const int zmq_version_major = ZMQ_VERSION_MAJOR;
const int zmq_version_minor = ZMQ_VERSION_MINOR;
const int zmq_version_patch = ZMQ_VERSION_PATCH;
const int zmq_version_num = ZMQ_VERSION;
const int zmq_defined_stdint = ZMQ_DEFINED_STDINT;
const int zmq_hausnumero = ZMQ_HAUSNUMERO;

// context options
const int zmq_io_threads = ZMQ_IO_THREADS;
const int zmq_max_sockets = ZMQ_MAX_SOCKETS;
const int zmq_socket_limit = ZMQ_SOCKET_LIMIT;
const int zmq_thread_priority = ZMQ_THREAD_PRIORITY;
const int zmq_thread_sched_policy = ZMQ_THREAD_SCHED_POLICY;

// context option defaults
const int zmq_io_threads_dflt = ZMQ_IO_THREADS_DFLT;
const int zmq_max_sockets_dflt = ZMQ_MAX_SOCKETS_DFLT;
const int zmq_thread_priority_dflt = ZMQ_THREAD_PRIORITY_DFLT;
const int zmq_thread_sched_policy_dflt = ZMQ_THREAD_SCHED_POLICY_DFLT;

// socket types
const int zmq_pair = ZMQ_PAIR;
const int zmq_pub = ZMQ_PUB;
const int zmq_sub = ZMQ_SUB;
const int zmq_req = ZMQ_REQ;
const int zmq_rep = ZMQ_REP;
const int zmq_dealer = ZMQ_DEALER;
const int zmq_router = ZMQ_ROUTER;
const int zmq_pull = ZMQ_PULL;
const int zmq_push = ZMQ_PUSH;
const int zmq_xpub = ZMQ_XPUB;
const int zmq_xsub = ZMQ_XSUB;
const int zmq_stream = ZMQ_STREAM;

// socket options
const int zmq_affinity = ZMQ_AFFINITY;
const int zmq_identity = ZMQ_IDENTITY;
const int zmq_subscribe = ZMQ_SUBSCRIBE;
const int zmq_unsubscribe = ZMQ_UNSUBSCRIBE;
const int zmq_rate = ZMQ_RATE;
const int zmq_recovery_ivl = ZMQ_RECOVERY_IVL;
const int zmq_sndbuf = ZMQ_SNDBUF;
const int zmq_rcvbuf = ZMQ_RCVBUF;
const int zmq_rcvmore = ZMQ_RCVMORE;
const int zmq_fd = ZMQ_FD;
const int zmq_events = ZMQ_EVENTS;
const int zmq_type = ZMQ_TYPE;
const int zmq_linger = ZMQ_LINGER;
const int zmq_reconnect_ivl = ZMQ_RECONNECT_IVL;
const int zmq_backlog = ZMQ_BACKLOG;
const int zmq_reconnect_ivl_max = ZMQ_RECONNECT_IVL_MAX;
const int zmq_maxmsgsize = ZMQ_MAXMSGSIZE;
const int zmq_sndhwm = ZMQ_SNDHWM;
const int zmq_rcvhwm = ZMQ_RCVHWM;
const int zmq_multicast_hops = ZMQ_MULTICAST_HOPS;
const int zmq_rcvtimeo = ZMQ_RCVTIMEO;
const int zmq_sndtimeo = ZMQ_SNDTIMEO;
const int zmq_last_endpoint = ZMQ_LAST_ENDPOINT;
const int zmq_router_mandatory = ZMQ_ROUTER_MANDATORY;
const int zmq_tcp_keepalive = ZMQ_TCP_KEEPALIVE;
const int zmq_tcp_keepalive_cnt = ZMQ_TCP_KEEPALIVE_CNT;
const int zmq_tcp_keepalive_idle = ZMQ_TCP_KEEPALIVE_IDLE;
const int zmq_tcp_keepalive_intvl = ZMQ_TCP_KEEPALIVE_INTVL;
const int zmq_immediate = ZMQ_IMMEDIATE;
const int zmq_xpub_verbose = ZMQ_XPUB_VERBOSE;
const int zmq_router_raw = ZMQ_ROUTER_RAW;
const int zmq_ipv6 = ZMQ_IPV6;
const int zmq_mechanism = ZMQ_MECHANISM;
const int zmq_plain_server = ZMQ_PLAIN_SERVER;
const int zmq_plain_username = ZMQ_PLAIN_USERNAME;
const int zmq_plain_password = ZMQ_PLAIN_PASSWORD;
const int zmq_curve_server = ZMQ_CURVE_SERVER;
const int zmq_curve_publickey = ZMQ_CURVE_PUBLICKEY;
const int zmq_curve_secretkey = ZMQ_CURVE_SECRETKEY;
const int zmq_curve_serverkey = ZMQ_CURVE_SERVERKEY;
const int zmq_probe_router = ZMQ_PROBE_ROUTER;
const int zmq_req_correlate = ZMQ_REQ_CORRELATE;
const int zmq_req_relaxed = ZMQ_REQ_RELAXED;
const int zmq_conflate = ZMQ_CONFLATE;
const int zmq_zap_domain = ZMQ_ZAP_DOMAIN;
const int zmq_router_handover = ZMQ_ROUTER_HANDOVER;
const int zmq_tos = ZMQ_TOS;
const int zmq_connect_rid = ZMQ_CONNECT_RID;
const int zmq_gssapi_server = ZMQ_GSSAPI_SERVER;
const int zmq_gssapi_principal = ZMQ_GSSAPI_PRINCIPAL;
const int zmq_gssapi_service_principal = ZMQ_GSSAPI_SERVICE_PRINCIPAL;
const int zmq_gssapi_plaintext = ZMQ_GSSAPI_PLAINTEXT;
const int zmq_handshake_ivl = ZMQ_HANDSHAKE_IVL;
const int zmq_socks_proxy = ZMQ_SOCKS_PROXY;
const int zmq_xpub_nodrop = ZMQ_XPUB_NODROP;

// message options
const int zmq_more = ZMQ_MORE;
const int zmq_srcfd = ZMQ_SRCFD;
const int zmq_shared = ZMQ_SHARED;

// send/recv options, flags!
const int zmq_dontwait = ZMQ_DONTWAIT;
const int zmq_sndmore = ZMQ_SNDMORE;

// security mechanisms
const int zmq_null = ZMQ_NULL;
const int zmq_plain = ZMQ_PLAIN;
const int zmq_curve = ZMQ_CURVE;
const int zmq_gssapi = ZMQ_GSSAPI;

// events, flags!
const int zmq_event_connected = ZMQ_EVENT_CONNECTED;
const int zmq_event_connect_delayed = ZMQ_EVENT_CONNECT_DELAYED;
const int zmq_event_connect_retried = ZMQ_EVENT_CONNECT_RETRIED;
const int zmq_event_listening = ZMQ_EVENT_LISTENING;
const int zmq_event_bind_failed = ZMQ_EVENT_BIND_FAILED;
const int zmq_event_accepted = ZMQ_EVENT_ACCEPTED;
const int zmq_event_accept_failed = ZMQ_EVENT_ACCEPT_FAILED;
const int zmq_event_closed = ZMQ_EVENT_CLOSED;
const int zmq_event_close_failed = ZMQ_EVENT_CLOSE_FAILED;
const int zmq_event_disconnected = ZMQ_EVENT_DISCONNECTED;
const int zmq_event_monitor_stopped = ZMQ_EVENT_MONITOR_STOPPED;
const int zmq_event_all = ZMQ_EVENT_ALL;

// poll options, flags!
const int zmq_pollin = ZMQ_POLLIN;
const int zmq_pollout = ZMQ_POLLOUT;
const int zmq_pollerr = ZMQ_POLLERR;


const int zmq_pollitems_dflt = ZMQ_POLLITEMS_DFLT;

const int zmq_has_capabilities = ZMQ_HAS_CAPABILITIES;

// posix errnos
const int e2big = E2BIG; // argument list too long
const int eacces = EACCES; // permission denied
const int eaddrinuse = EADDRINUSE; // address already in use
const int eaddrnotavail = EADDRNOTAVAIL; // address not available
const int eafnosupport = EAFNOSUPPORT; // address family not supported
const int eagain = EAGAIN; // resource temporarily unavailable (may be the same value as ewouldblock)
const int ealready = EALREADY; // connection already in progress
const int ebadf = EBADF; // bad file descriptor
const int ebadmsg = EBADMSG; // bad message
const int ebusy = EBUSY; // device or resource busy
const int ecanceled = ECANCELED; // operation canceled
const int echild = ECHILD; // no child processes
const int econnaborted = ECONNABORTED; // connection aborted
const int econnrefused = ECONNREFUSED; // connection refused
const int econnreset = ECONNRESET; // connection reset
const int edeadlk = EDEADLK; // resource deadlock avoided
const int edestaddrreq = EDESTADDRREQ; // destination address required
const int edom = EDOM; // mathematics argument out of domain of function
const int edquot = EDQUOT; // disk quota exceeded
const int eexist = EEXIST; // file exists
const int efault = EFAULT; // bad address
const int efbig = EFBIG; // file too large
const int ehostunreach = EHOSTUNREACH; // host is unreachable
const int eidrm = EIDRM; // identifier removed
const int eilseq = EILSEQ; // illegal byte sequence
const int einprogress = EINPROGRESS; // operation in progress
const int eintr = EINTR; // interrupted function call
const int einval = EINVAL; // invalid argument
const int eio = EIO; // input/output error
const int eisconn = EISCONN; // socket is connected
const int eisdir = EISDIR; // is a directory
const int eloop = ELOOP; // too many levels of symbolic links
const int emfile = EMFILE; // too many open files
const int emlink = EMLINK; // too many links
const int emsgsize = EMSGSIZE; // message too long
const int emultihop = EMULTIHOP; // multihop attempted
const int enametoolong = ENAMETOOLONG; // filename too long
const int enetdown = ENETDOWN; // network is down
const int enetreset = ENETRESET; // connection aborted by network
const int enetunreach = ENETUNREACH; // network unreachable
const int enfile = ENFILE; // too many open files in system
const int enobufs = ENOBUFS; // no buffer space available
const int enodata = ENODATA; // no message is available on the stream head read queue
const int enodev = ENODEV; // no such device
const int enoent = ENOENT; // no such file or directory
const int enoexec = ENOEXEC; // exec format error
const int enolck = ENOLCK; // no locks available
const int enolink = ENOLINK; // link has been severed
const int enomem = ENOMEM; // not enough space
const int enomsg = ENOMSG; // no message of the desired type
const int enoprotoopt = ENOPROTOOPT; // protocol not available
const int enospc = ENOSPC; // no space left on device
const int enosr = ENOSR; // no stream resources
const int enostr = ENOSTR; // not a stream
const int enosys = ENOSYS; // function not implemented
const int enotconn = ENOTCONN; // the socket is not connected
const int enotdir = ENOTDIR; // not a directory
const int enotempty = ENOTEMPTY; // directory not empty
const int enotsock = ENOTSOCK; // not a socket
const int enotsup = ENOTSUP; // operation not supported
const int enotty = ENOTTY; // inappropriate i/o control operation
const int enxio = ENXIO; // no such device or address
const int eopnotsupp = EOPNOTSUPP; // operation not supported on socket
const int eoverflow = EOVERFLOW; // value too large to be stored in data type
const int eperm = EPERM; // operation not permitted
const int epipe = EPIPE; // broken pipe
const int eproto = EPROTO; // protocol error
const int eprotonosupport = EPROTONOSUPPORT; // protocol not supported
const int eprototype = EPROTOTYPE; // protocol wrong type for socket
const int erange = ERANGE; // result too large
const int erofs = EROFS; // read-only filesystem
const int espipe = ESPIPE; // invalid seek
const int esrch = ESRCH; // no such process
const int estale = ESTALE; // stale file handle
const int etime = ETIME; // timer expired
const int etimedout = ETIMEDOUT; // connection timed out
const int etxtbsy = ETXTBSY; // text file busy
const int ewouldblock = EWOULDBLOCK; // operation would block (may be same value as eagain)
const int exdev = EXDEV; // improper link

// zmq errnos
const int efsm = EFSM;
const int enocompatproto = ENOCOMPATPROTO;
const int eterm = ETERM;
const int emthread = EMTHREAD;
