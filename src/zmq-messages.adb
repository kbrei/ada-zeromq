with Ada.Unchecked_Conversion;
with System.Address_To_Access_Conversions;
with Interfaces.C.Pointers;

package body ZMQ.Messages is

   use ZMQ.Raw;

   function Option_To_Int is new Ada.Unchecked_Conversion
     (Source => Message_Option, Target => int);

   ------------
   -- Create --
   ------------

   function Create return Message is
      Raw_Message : zmq_msg_t;
      Result : int := 0;
   begin
      Result := zmq_msg_init (Raw_Message);

      if Check (Result) = Not_Enough_Space then
         raise Storage_Error with
           "Error while allocating ZMQ.Messages.Message: Out of memory.";
      end if;

      return Message (Raw_Message);
   end Create;

   ------------
   -- Create --
   ------------

   function Create
     (Size : ZMQ.Size)
      return Message
   is
      Raw_Message : zmq_msg_t;
      Result : int := 0;
   begin
      Result := zmq_msg_init_size (Raw_Message, size_t (Size));

      if Check (Result) = Not_Enough_Space then
         raise Storage_Error with
           "Error while allocating ZMQ.Messages.Message: Out of memory.";
      end if;

      return Message (Raw_Message);
   end Create;

   ------------
   -- Create --
   ------------

   function Create
     (Data : not null access Bytes;
      Clean_Up : Free_Callback := null;
      Hint : System.Address := System.Null_Address)
      return Message
   is
      Raw_Message : zmq_msg_t;
      Result : int := 0;
   begin
      Result := zmq_msg_init_data
        (msg => Raw_Message,
         data => Data.all'Address,
         size => Data.all'Length,
         free => Clean_Up,
         hint => Hint);

      if Check (Result) = Not_Enough_Space then
         raise Storage_Error with
           "Error while allocating ZMQ.Messages.Message: Out of memory.";
      end if;

      return Message (Raw_Message);
   end Create;

   ----------
   -- Move --
   ----------

   function Move
     (To : in out Message;
      From : in out Message)
      return Error
   is
      Result : int := 0;
   begin
      Result := zmq_msg_move
        (dest => zmq_msg_t (To),
         src => zmq_msg_t (From));

      return Check (Result);
   end Move;

   ----------
   -- Copy --
   ----------

   function Copy
     (To : in out Message;
      From : in out Message)
      return Error
   is
      Result : int := 0;
   begin
      Result := zmq_msg_copy
        (dest => zmq_msg_t (To),
         src => zmq_msg_t (From));

      return Check (Result);
   end Copy;

   ----------
   -- More --
   ----------

   function More
     (Message : in out ZMQ.Messages.Message)
      return Boolean
   is
      Result : int := 0;
   begin
      Result := zmq_msg_more (zmq_msg_t (Message));
      return (if Result = 1 then True else False);
   end More;

   ----------
   -- Data --
   ----------

   function Data
     (Message : in out ZMQ.Messages.Message)
     return Bytes
   is
      package Byte_Ptrs is new Interfaces.C.Pointers
        (Index => Unsigned_Positive,
         Element => Byte,
         Element_Array => Bytes,
         Default_Terminator => 0);
      use Byte_Ptrs;

      package Byte_Conv is new System.Address_To_Access_Conversions
        (Object => Byte);

      Data_Base_Address : System.Address := System.Null_Address;
      Data_Size : size_t := 0;

      Data_Pointer : Byte_Ptrs.Pointer := null;

      Empty : constant Bytes (1 .. 0) := (others => 0);
   begin
      Data_Size := zmq_msg_size (zmq_msg_t (Message));
      Data_Base_Address := zmq_msg_data (zmq_msg_t (Message));

      Data_Pointer :=
        Byte_Ptrs.Pointer (Byte_Conv.To_Pointer (Data_Base_Address));

      if Data_Pointer = null or Data_Size = 0 then
         return Empty;
      else
         return Byte_Ptrs.Value (Data_Pointer, ptrdiff_t (Data_Size));
      end if;
   end Data;

   -----------
   --  Get  --
   -----------

   function Get
     (Message : in out ZMQ.Messages.Message;
      Option : Boolean_Message_Option)
     return Boolean
   is
      Result : int := 0;
      Int_Option : constant int :=
        Option_To_Int (Message_Option (Option));
   begin
      Result := zmq_msg_get
        (msg => zmq_msg_t (Message),
         property => Int_Option);

      return (if Result = 1 then True else False);
   end Get;

   -----------
   --  Get  --
   -----------

   function Get
     (Message : in out ZMQ.Messages.Message;
      Option : OS_Socket_Message_Option)
     return OS_Socket
   is
      Result : int;
      Int_Option : constant int :=
        Option_To_Int (Message_Option (Option));
   begin
      Result := zmq_msg_get
        (msg => zmq_msg_t (Message),
         property => Int_Option);

      return OS_Socket (Result);
   end Get;

   -----------
   --  Set  --
   -----------

   procedure Set
     (Message : in out ZMQ.Messages.Message;
      Option : Boolean_Message_Option;
      Value : Boolean)
   is
      Result : int with Unreferenced;
      Int_Option : constant int :=
        Option_To_Int (Message_Option (Option));
   begin
      Result := zmq_msg_set
        (msg => zmq_msg_t (Message),
         property => Int_Option,
         optval => (if Value then 1 else 0));
   end Set;

   -----------
   --  Set  --
   -----------

   procedure Set
     (Message : in out ZMQ.Messages.Message;
      Option : OS_Socket_Message_Option;
      Value : OS_Socket)
   is
      Result : int with Unreferenced;
      Int_Option : constant int :=
        Option_To_Int (Message_Option (Option));
   begin
      Result := zmq_msg_set
        (msg => zmq_msg_t (Message),
         property => Int_Option,
         optval => int (Value));
   end Set;

   ------------
   --  Send  --
   ------------

   function Send
     (Socket : ZMQ.Sockets.Socket;
      Message : in out ZMQ.Messages.Message;
      Flags : Send_Recv_Flags;
      Bytes_Send : out Integer)
     return Error
   is
      function Flags_To_Int is new Ada.Unchecked_Conversion
        (Source => Send_Recv_Flags, Target => int);

      Result : int;
   begin
      Result := zmq_msg_send
        (msg => zmq_msg_t (Message),
         s => System.Address (Socket),
         flags => Flags_To_Int (Flags));

      --  TODO: this works on GNAT but not nessecairly
      --  on other compilers
      Bytes_Send := Integer (Result);

      return Check (Result);
   end Send;

   ---------------
   --  Receive  --
   ---------------

   function Receive
     (Socket : ZMQ.Sockets.Socket;
      Message : in out ZMQ.Messages.Message;
      Flags : Send_Recv_Flags;
      Bytes_Received : out Integer)
     return Error
   is
      function Flags_To_Int is new Ada.Unchecked_Conversion
        (Source => Send_Recv_Flags, Target => int);

      Result : int;
   begin
      Result := zmq_msg_recv
        (msg => zmq_msg_t (Message),
         s => System.Address (Socket),
         flags => Flags_To_Int (Flags));

      --  TODO: this works on GNAT but not nessecairly
      --  on other compilers
      Bytes_Received := Integer (Result);

      return Check (Result);
   end Receive;

end ZMQ.Messages;
