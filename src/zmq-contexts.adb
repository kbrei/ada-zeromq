with ZMQ.Raw; use ZMQ.Raw;
with System;
with Ada.Unchecked_Conversion;

package body ZMQ.Contexts is

   function Option_To_Int is new Ada.Unchecked_Conversion
     (Source => Context_Option, Target => int);

   ------------
   -- Create --
   ------------

   function Create return Context
   is
      New_Context : constant System.Address := zmq_ctx_new;
   begin
      return Context (New_Context);
   end Create;

   ----------
   -- Term --
   ----------

   function Term
     (Context : ZMQ.Contexts.Context)
      return Error
   is
      Result : constant int := zmq_ctx_term (System.Address (Context));
   begin
      return Check (Result);
   end Term;

   --------------
   -- Shutdown --
   --------------

   function Shutdown
     (Context : ZMQ.Contexts.Context)
      return Error
   is
      Result : constant int := zmq_ctx_shutdown (System.Address (Context));
   begin
      return Check (Result);
   end Shutdown;

   ---------
   -- Set --
   ---------

   function Set
     (Context : ZMQ.Contexts.Context;
      Option : Context_Option;
      Value : Context_Value)
     return Error
   is
      Result : constant int := zmq_ctx_set
        (context => System.Address (Context),
         option => Option_To_Int (Option),
         optval => int (Value));
   begin
      return Check (Result);
   end Set;

   ---------
   -- Get --
   ---------

   function Get
     (Context : ZMQ.Contexts.Context;
      Option : Context_Option;
      Error : out ZMQ.Errno.Error)
     return Context_Value
   is
      Result : constant int := zmq_ctx_get
        (context => System.Address (Context),
         option => Option_To_Int (Option));

   begin
      Error := Check (Result);

      return Context_Value (Result);
   end Get;

end ZMQ.Contexts;
