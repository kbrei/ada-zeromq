with Ahven; use Ahven;
with Ahven.Framework; use Ahven.Framework;
with Ahven.Text_Runner;

pragma Elaborate_All
  (Ahven,
   Ahven.Framework,
   Ahven.Text_Runner);

procedure Run_Test
is
   Suite : Ahven.Framework.Test_Suite :=
     Ahven.Framework.Create_Suite ("All");

begin
   --  Ahven.Framework.Add_Test (Suite, new Regressions.Test);
   Ahven.Text_Runner.Run (Suite);
end Run_Test;
